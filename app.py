from flask import Flask, render_template, request
import string
import os
import random

app = Flask(__name__)


routes_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'urls.cvs')

@app.route('/')
def main():
    return render_template('main.html')

@app.route('/add', methods=['POST'])
def add_post():
    new_url = request.form.get('url')

    while True:
        new_code = generate_code()
        if save_new_code(new_url, new_code):
            break

    return request.host_url + new_code

def generate_code():
    return ''.join(random.sample(string.ascii_letters, 6))

def save_new_code(new_url, new_code):
    if os.path.exists(routes_file):
        with open(routes_file) as fh:
            for line in fh:
                line = line.rstrip('\n')
                url, code = line.split(';')
                if code == new_code:
                    return False
    with open(routes_file, 'a') as fh:
        fh.write(f"{new_url};{new_code}\n")
    return True


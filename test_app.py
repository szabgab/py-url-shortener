import app
import re

def test_app():
    web = app.app.test_client()

    rv = web.get('/')
    assert rv.status_code == 200
    assert b'<h1>Welcome to the URL shortener Demo</h1>' in rv.data

    rv = web.post('/add', data={'url': 'http://examples.org'})
    assert rv.status_code == 200
    assert re.search(r'^http://localhost/.{6}$', rv.data.decode('utf-8'))
    #print(rv.data)



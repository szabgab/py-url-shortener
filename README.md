# URL Shortener demo

Specification: https://code-maven.com/url-shortener

## Try it

```
pip install requirements.txt
flask run
```

## Develop


```
FLASK_APP=app FLASK_DEBUG=1 flask run
```